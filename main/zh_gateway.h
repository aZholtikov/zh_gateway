#pragma once

#include "stdio.h"
#include "string.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "esp_err.h"
#include "driver/gpio.h"
#include "esp_eth.h"
#include "esp_timer.h"
#include "esp_sntp.h"
#include "esp_app_format.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_mac.h"
#include "mqtt_client.h"
#include "zh_espnow.h"
#include "zh_network.h"
#include "zh_json.h"
#include "zh_config.h"

#ifdef CONFIG_NETWORK_TYPE_DIRECT
#define zh_send_message(a, b, c) zh_espnow_send(a, b, c)
#define ZH_EVENT ZH_ESPNOW
#else
#define zh_send_message(a, b, c) zh_network_send(a, b, c)
#define ZH_EVENT ZH_NETWORK
#endif

#ifdef CONFIG_IDF_TARGET_ESP32
#define ZH_CHIP_TYPE HACHT_ESP32
#elif CONFIG_IDF_TARGET_ESP32S2
#define ZH_CHIP_TYPE HACHT_ESP32S2
#elif CONFIG_IDF_TARGET_ESP32S3
#define ZH_CHIP_TYPE HACHT_ESP32S3
#elif CONFIG_IDF_TARGET_ESP32C2
#define ZH_CHIP_TYPE HACHT_ESP32C2
#elif CONFIG_IDF_TARGET_ESP32C3
#define ZH_CHIP_TYPE HACHT_ESP32C3
#elif CONFIG_IDF_TARGET_ESP32C6
#define ZH_CHIP_TYPE HACHT_ESP32C6
#endif

#define ZH_LAN_MODULE_TYPE(x) esp_eth_phy_new_rtl8201(x) // Change it according to the LAN module used.
#define ZH_LAN_MODULE_POWER_PIN GPIO_NUM_12              // Change it according to the LAN module used.

#define ZH_WIFI_MAXIMUM_RETRY 5  // Maximum number of unsuccessful WiFi connection attempts.
#define ZH_WIFI_RECONNECT_TIME 5 // Waiting time (in seconds) between attempts to reconnect to WiFi (if number of attempts of unsuccessful connections is exceeded).
#define MAC_STR "%02X-%02X-%02X-%02X-%02X-%02X"

#define ZH_MESSAGE_TASK_PRIORITY 2
#define ZH_MESSAGE_STACK_SIZE 3072
#define ZH_SNTP_TASK_PRIORITY 2
#define ZH_SNTP_STACK_SIZE 2048
#define ZH_OTA_TASK_PRIORITY 3
#define ZH_OTA_STACK_SIZE 8192

#define ZH_MAX_GPIO_NUMBERS 48 // Maximum number of GPIOs on ESP modules.

typedef struct gateway_config_t // Structure of data exchange between tasks, functions and event handlers.
{
    uint8_t self_mac[ESP_NOW_ETH_ALEN];             // Gateway MAC address. Depends at WiFi operation mode.
    bool sntp_is_enable;                            // SNTP client operation status flag. Used to control the SNTP functions when the network connection is established/lost.
    bool mqtt_is_enable;                            // MQTT client operation status flag. Used to control the MQTT functions when the network connection is established/lost.
    bool mqtt_is_connected;                         // MQTT broker connection status flag. Used to control the gateway system tasks when the MQTT connection is established/lost.
    esp_timer_handle_t wifi_reconnect_timer;        // Unique WiFi reconnection timer handle. Used when the number of attempts of unsuccessful connections is exceeded.
    uint8_t wifi_reconnect_retry_num;               // System counter for the number of unsuccessful WiFi connection attempts.
    esp_mqtt_client_handle_t mqtt_client;           // Unique MQTT client handle.
    TaskHandle_t gateway_attributes_message_task;   // Unique task handle for zh_gateway_send_mqtt_json_attributes_message_task().
    TaskHandle_t gateway_keep_alive_message_task;   // Unique task handle for zh_gateway_send_mqtt_json_keep_alive_message_task().
    TaskHandle_t gateway_current_time_task;         // Unique task handle for zh_send_espnow_current_time_task().
    zh_espnow_ota_data_t espnow_ota_data;           // Structure for initial transfer system data to the node update task.
    SemaphoreHandle_t espnow_ota_data_semaphore;    // Semaphore for control the acknowledgement of successful receipt of an update package from a node.
    SemaphoreHandle_t self_ota_in_progress_mutex;   // Mutex blocking the second run of the gateway update task.
    SemaphoreHandle_t espnow_ota_in_progress_mutex; // Mutex blocking the second run of the node update task.
} gateway_config_t;

extern const uint8_t server_certificate_pem_start[] asm("_binary_certificate_pem_start");
extern const uint8_t server_certificate_pem_end[] asm("_binary_certificate_pem_end");

#ifdef CONFIG_CONNECTION_TYPE_LAN
// Function for LAN event processing.
static void zh_eth_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
#else
// Function for WiFi event processing.
static void zh_wifi_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
#endif
// Function for ESP-NOW event processing.
static void zh_espnow_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
// Function for MQTT event processing.
static void zh_mqtt_event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

// Task of updating the gateway firmware.
static void zh_self_ota_update_task(void *pvParameter);
// Task of updating the any esp-now node firmware.
static void zh_espnow_ota_update_task(void *pvParameter);

// The task for getting the current time from the internet and sending it to the all esp-now nodes.
static void zh_send_espnow_current_time_task(void *pvParameter);

// Function for checking the correctness of the GPIO number value.
static uint8_t zh_gpio_number_check(int gpio);
// Function for checking the correctness of the bool variable value.
static bool zh_bool_value_check(int value);
// Function for checking the correctness of the sensor type value.
static uint8_t zh_sensor_type_check(int type);
// Function for checking the correctness of the uint16_t variable value.
static uint16_t zh_uint16_value_check(int value);

// Task for prepare the attributes message from a gateway and transfer it to the processing function zh_espnow_send_mqtt_json_attributes_message().
static void zh_gateway_send_mqtt_json_attributes_message_task(void *pvParameter);
// Function for prepare the configuration message from a gateway and transfer it to the processing function zh_espnow_binary_sensor_send_mqtt_json_config_message().
static void zh_gateway_send_mqtt_json_config_message(const gateway_config_t *gateway_config);
// Task for prepare the keep alive message from a gateway, transfer it to the processing function zh_espnow_send_mqtt_json_keep_alive_message() and sending to all esp-now nodes.
static void zh_gateway_send_mqtt_json_keep_alive_message_task(void *pvParameter);

// Function for converting to JSON and sending to the MQTT broker the attributes message received from a any esp-now node.
static void zh_espnow_send_mqtt_json_attributes_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);
// Function for converting to JSON and sending to the MQTT broker the keep alive message received from a any esp-now node.
static void zh_espnow_send_mqtt_json_keep_alive_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);

// Function for converting to JSON and sending to the MQTT broker the configuration message received from a zh_espnow_switch node.
static void zh_espnow_switch_send_mqtt_json_config_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);
// Function for converting to JSON and sending to the MQTT broker the hardware configuration message received from a zh_espnow_switch node.
static void zh_espnow_switch_send_mqtt_json_hardware_config_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);
// Function for converting to JSON and sending to the MQTT broker the status message received from a zh_espnow_switch node.
static void zh_espnow_switch_send_mqtt_json_status_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);

// Function for converting to JSON and sending to the MQTT broker the configuration message received from a zh_espnow_led node.
static void zh_espnow_led_send_mqtt_json_config_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);
// Function for converting to JSON and sending to the MQTT broker the status message received from a zh_espnow_led node.
static void zh_espnow_led_send_mqtt_json_status_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);

// Function for converting to JSON and sending to the MQTT broker the configuration message received from a zh_espnow_sensor node.
static void zh_espnow_sensor_send_mqtt_json_config_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);
// Function for converting to JSON and sending to the MQTT broker the hardware configuration message received from a zh_espnow_sensor node.
static void zh_espnow_sensor_send_mqtt_json_hardware_config_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);
// Function for converting to JSON and sending to the MQTT broker the status message received from a zh_espnow_sensor node.
static void zh_espnow_sensor_send_mqtt_json_status_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);

// Function for converting to JSON and sending to the MQTT broker the configuration message received from a zh_espnow_binary_sensor node.
static void zh_espnow_binary_sensor_send_mqtt_json_config_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);
// Function for converting to JSON and sending to the MQTT broker the status message received from a zh_espnow_binary_sensor node.
static void zh_espnow_binary_sensor_send_mqtt_json_status_message(zh_espnow_data_t *device_data, const uint8_t *device_mac, const gateway_config_t *gateway_config);